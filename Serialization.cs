﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SixthLessonOTUS_8
{
    public static class Serialization
    {
        private static readonly char NameValueDevider = ':';
        private static readonly char EndNameValue = '.';

        public static string Serialize(this object obj) => string.Join("\n", obj.GetType().GetProperties().Select(x => x.Name + NameValueDevider + x.GetValue(obj) + EndNameValue));
        public static T Deserialize<T>(string str) where T : new()
        {
            T result = new T();
            int i = 0;

            var mass = str.ToCharArray();
            var properties = result.GetType().GetProperties().ToList();

            StringBuilder world = new StringBuilder();
            foreach (var property in properties)
            {
                while (mass[i] != NameValueDevider)
                {
                    i++;
                }
                if (mass[i] != NameValueDevider)
                    throw new ArgumentException();
                i++;
                while (mass[i] != EndNameValue)
                {
                    world.Append(mass[i]);
                    i++;
                }
                if (mass[i] != EndNameValue)
                    throw new ArgumentException();
                var propertyType = property.PropertyType;
                if (propertyType == typeof(string))
                    result.GetType().GetProperty(property.Name).SetValue(result, world.ToString());
                else
                    result.GetType().GetProperty(property.Name).SetValue(result, Convert.ToByte(world.ToString()));
                world.Clear();
            }
            return result;
        }
    }
}
