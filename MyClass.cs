﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SixthLessonOTUS_8
{
    public class MyClass
    {
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public byte Age { get; set; }
    }
}
