﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SixthLessonOTUS_8
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 0;

            MyClass myClass = new MyClass
            {
                FirstName = "Evgeny",
                SecondName = "Valavin",
                Age = 20
            };

            byte y = 1;
            long avr = 0;

            Console.WriteLine("Personal serializer:");
            while (y <= 3)
            {
                i = 0;
                var stopwatch = Stopwatch.StartNew();
                while (i != 1_000_000)
                {
                    myClass.Serialize();
                    i++;
                }
                stopwatch.Stop();
                Console.WriteLine("Elapsed time in milliseconds: " + stopwatch.ElapsedMilliseconds + "");
                avr += stopwatch.ElapsedMilliseconds;
                y++;
            }
            var stopwatch2 = Stopwatch.StartNew();
            Console.WriteLine("Average time needed to serialize: " + avr / 3 + " milliseconds\n");
            stopwatch2.Stop();
            y = 1;
            avr = 0;
            Console.WriteLine("Netonsoft's serializer:");
            while (y <= 3)
            {
                i = 0;
                var stopwatch = Stopwatch.StartNew();
                while (i != 1_000_000)
                {
                    JsonConvert.SerializeObject(myClass);
                    i++;
                }
                stopwatch.Stop();
                Console.WriteLine("Elapsed time in milliseconds: " + stopwatch.ElapsedMilliseconds + "");
                avr += stopwatch.ElapsedMilliseconds;
                y++;
            }
            Console.WriteLine("Average time needed to serialize: " + avr / 3 + " milliseconds\n");
            Console.WriteLine("It took " + stopwatch2.Elapsed + " to print the average time in console\n\n");

            y = 1;
            avr = 0;
            Console.WriteLine("Personal deserializer:");
            while (y <= 3)
            {
                i = 0;
                var stopwatch = Stopwatch.StartNew();
                while (i != 1_000_000)
                {
                    Serialization.Deserialize<MyClass>(myClass.Serialize());
                    i++;
                }
                stopwatch.Stop();
                Console.WriteLine("Elapsed time in milliseconds: " + stopwatch.ElapsedMilliseconds + "");
                avr += stopwatch.ElapsedMilliseconds;
                y++;
            }
            Console.WriteLine("Average time needed to deserialize: " + avr / 3 + " milliseconds\n");
            y = 1;
            avr = 0;
            Console.WriteLine("Netonsoft's deserializer:");
            while (y <= 3)
            {
                i = 0;
                var stopwatch = Stopwatch.StartNew();
                while (i != 1_000_000)
                {
                    JsonConvert.DeserializeObject<MyClass>(JsonConvert.SerializeObject(myClass));
                    i++;
                }
                stopwatch.Stop();
                Console.WriteLine("Elapsed time in milliseconds: " + stopwatch.ElapsedMilliseconds + "");
                avr += stopwatch.ElapsedMilliseconds;
                y++;
            }
            Console.WriteLine("Average time needed to deserialize: " + avr / 3 + " milliseconds\n");
            Console.WriteLine("***DONE***");

            Console.ReadLine();
        }
    }
}
